FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.17.0 as zig-source
COPY "install_zig_stable.sh" "install_zig.sh"
COPY "version_manifest.txt" "version_manifest.txt"
COPY "parse_zig_version.sh" "parse_zig_version.sh"
RUN apk add --no-cache "tar" "xz" "curl" "jq"
RUN sh "install_zig.sh"

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.17.0
COPY --from=zig-source "/usr/bin/zig" "/usr/bin/zig"
COPY --from=zig-source "/usr/lib/zig" "/usr/lib/zig"
COPY --from=zig-source "/usr/share/licenses/zig/LICENSE" "/usr/share/licenses/zig/LICENSE"
